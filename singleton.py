# class singleton:
#     instance = None
#     @staticmethod
#     def getInstance():
#         if singleton.instance is None:
#             singleton()
#         return singleton.instance
#     def __init__(self):
#         if singleton.instance!= None:
#             raise Exception("This is singleton class")
#         else:
#             singleton.instance=self
# sinle = singleton()
# print(sinle)
#
#
# sinle=singleton.getInstance()
# print(sinle)

class singleTon(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(singleTon, cls).__call__(*args, **kwargs)
        return cls._instances


class A(metaclass=singleTon):
    def __init__(self):
        pass

    def methodA(self):
        print("A")


a = A()
print(a)
a1 = A()
print(a1)
