class A(object):
    def __init__(self):
        pass
    def methodA(self):
        print("A")
class B(object):
    def __init__(self):
        pass
    def methodB(self):
        print("B")

#Factory Method
def get(obj):
    objs=dict(a=A(),b=B())
    return objs[obj]

class objCreation:
    def createObj(self, inputString):
        createdPiece = get(inputString)
        return createdPiece

clsobj = objCreation()
finalobj=clsobj.createObj('a')
finalobj.methodA()
# a=get('a')
# a.methodA()
# b=get('b')
# b.methodB()